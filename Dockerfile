FROM jupyter/datascience-notebook

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
RUN echo "c.NotebookApp.default_url = '/lab'" >> /home/jovyan/.jupyter/jupyter_notebook_config.py
