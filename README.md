# jupyterlab-datascience-image
A docker-compose setup for https://hub.docker.com/r/jupyter/datascience-notebook that also incudes the jupyterlab ui

## General Info:
This image is based on the jupyter/datascience docker image. It includes a python and an R kernel, as well as many of the commonly used data-science libraries.

## prerequisites

 - install docker
 - instlal docker-compose

## start the notebook server

 ```
 docker-compose up
 ```

 then look for the URL in the consle (of the form `http://127.0.0.1:8888/?token=df442a53d6d4a710291779f1b69951245893c909b9f290eb`)

## Add your data
Copy your datasets to the `/data` folder. You can then find your data from the jupyter-notebook file explorer

## Add existing notebooks
Either upload the notebook using the browser, or copy the notebook to the `/notebooks` folder


## Install your own dependencies
For python dependencies, add them to the `requirements.txt` file and then run `docker-compose up --build`


